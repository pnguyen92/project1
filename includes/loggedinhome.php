<?php
	$week=!($_GET)?"":"true";	
	$var=!$_GET?'today':"this Week";
?> 

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
	  		<div class="panel-heading">
	  			Your Summary
  				<ul class="nav nav-tabs">
				    <li role="presentation" <?php echo  (!($_GET)?'':"class='active'")?> ><a href="/index.php?week">Weekly</a></li>
				    <li role="presentation" <?php echo  (($_GET)?'':"class='active'")?>><a href="/index.php">Daily</a></li>
				</ul>
	  		</div>
	  			<div class="panel-body">
	    			<div class="col-xs-12 col-sm-3 col-md-2 ">
	    				<img src="/static/img/user_img.png" style="height:100px" alt="userimg">
	    			</div>
	    			<div class="col-xs-12 col-sm-8 col-lg-10">
	    				<?php 
	    					include_once "/classes/userobj.php";
							include_once "/classes/fooditem.php";
							include_once "/classes/exerciseitem.php";
	    					if ($stmt = $con->prepare("SELECT male,activitylvl,goalweight,dob,weight,height
	    					FROM users
	    					where id =?
					        LIMIT 1")) {
					        $stmt->bind_param('i', $_SESSION['user_id']);  // Bind "$email" to parameter.

					        $stmt->execute();    // Execute the prepared query.
					        $stmt->store_result();
					        // get variables from result.
					        $stmt->bind_result($male, $activitylvl,$gw,$dob,$weight,$height);
					        $stmt->fetch();
					        }

	    					$user = new user;
	    					$user->setGender($male);
	    					$user->setCurrWeight($weight);
	    					$user->setDOB($dob);
	    					$user->setheight($height);
	    					$user->setActivityLvl($activitylvl);
	    					/*echo 'userid:'.$_SESSION['user_id'].'<br/>';
	    					echo 'ActivityLvl:'.$user->getActivityLvl($activitylvl).'<br/>';
	    					echo  'height  :'.$user->getheight().'<br/>';
	    					echo  'weight  :'.$user->getcurrweight().'<br/>';
	    					echo  'dob  :'.$user->getdob().'<br/>';
	    					echo  'gender  :'.$user->getGender().'<br/>';
	    					echo 'bmr :'.$user->getbmr().'<br/>';
	    					echo 'goal :'.$user-> getallowancecalories().'<br/>';*/
	    					$goal=($week=="true")?$user-> getallowancecalories()*7:$user-> getallowancecalories();
	    					include "/libs/modules/foodcalories.php";
	    					include "/libs/modules/exercisecalories.php";
	    					$remaining=($goal-($foodtotal-$exercisetotal));
	    					$percent=round((($foodtotal-$exercisetotal)/$goal)*100);
	    					$remainingtext= $remaining<=0?"you have exceded your calorie limit for ".$var:$remaining.' calories remaining for '.$var;
	    					echo $remainingtext;

	    				?>
						
						<div class="pull-right">
						<a href="/food/index.php" class=" btn btn-default">Add Food</a>
						<a href="/exercise/index.php" class=" btn btn-default">Add Exercise</a>
					</div>
	    			</div>
	    			<div class="col-xs-8  col-md-10">
	    				<div class="col-xs-12  col-sm-4 col-md-2">
	    					<h4> <?php echo $goal; ?> Calorie Goal  </h4>  
	    				</div>
	    				<div class="col-xs-12 col-sm-8 col-md-10">
	    					<h4>
	    						<div class="row">
	    							<div class="col-xs-12">
		    							<?php echo $foodtotal; ?> calories from Food
		    						</div>
		    						<div class="col-xs-12">
		    						 	- <?php echo $exercisetotal; ?> calories from Exercise
		    						</div>
		    						<div class="col-xs-12">
		    							 =  <?php echo ($foodtotal-$exercisetotal);?> Net calories
		    						</div>
	    						</div>
	    					</h4>
	    				</div>
	    			</div>
	    			<div class="col-xs-12 padding-top">

	    				<div class="progress">
						  	<div class="progress-bar <?php echo ($percent >75?'progress-bar-danger':'progress-bar-success') ?>"  role="progressbar" aria-valuenow="20" area-valuenow="<?php echo $percent  ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 20px; width: <?php echo ($percent >100?100:$percent);  ?>%;">
						    	<?php echo ($percent >100?100:$percent); ?>%
						 	</div>
						</div>
	    			</div>
	 			</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="panel panel-default">
		  		<div class="panel-heading"> 
		  			Food Summary
		  		</div>
		  		<div class="panel-body">
		  			<?php
		  					$sum = 0;
	    					echo "<table class='table table-striped'><tr>";
        					echo "<th>Name</th><th>Description</th><th>Calories</th><th>Date</th><th>Servings</th></tr>";
        					foreach ($user->getFoods() as $val) {
        						echo "<tr>";
        						echo "<td>".$val->getName()."</td>";
            					echo "<td>".$val->getDescription()."</td>";
            					echo "<td>".$val->getCalories()."</td>";
            					echo "<td>".$val->getDate()."</td>";
            					echo "<td>".$val->getServings()."</td>";
            					echo "</tr>";
            					$sum += $val->getCalories()*$val->getServings();
        					}
        					echo "</table>";
        					echo $sum." calories total consumed  ".$var;
	    				?>
		 		</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Exercise Summary
				</div>
				<div class="panel-body">
					<?php
		  					$tot = 0;
	    					echo "<table class='table table-striped'><tr>";
        					echo "<th>Name</th><th>Description</th><th>Calories Burned</th><th>Date</th><th>Increments</th></tr>";
        					foreach ($user->getExercises() as $val) {
        						echo "<tr>";
        						echo "<td>".$val->getName()."</td>";
            					echo "<td>".$val->getDescription()."</td>";
            					echo "<td>".$val->getCalories()."</td>";
            					echo "<td>".$val->getDate()."</td>";
            					echo "<td>".$val->getIncrements()."</td>";
            					echo "</tr>";
            					$tot += $val->getCalories()*$val->getIncrements();
        					}
        					echo "</table>";
        					echo $tot." calories total burned  ".$var;
	    				?>				</div>
			</div>
		</div>
	</div>
</div>


