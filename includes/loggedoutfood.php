<?php
$scripts="/static/js/select2.min.js,/static/js/foodindex.js";
$stylesheets="/static/css/select2.css";
include $_SERVER['DOCUMENT_ROOT']."/classes/food.php";
//include $_SERVER['DOCUMENT_ROOT'].'/includes/header.php';
//include $_SERVER['DOCUMENT_ROOT'].'/auth/functions.php';

?>
<div class="row">
	<div id="error" class="col-xs-12 hide">
	</div>
	<div class="col-xs-12 col-lg-12 well">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
			    <h3>Search  food</h3>
			    <p>Enter a food name to search our database</p>
			</div>
		</div>
	    
	  	<div class="row">
		    <form   method="GET"  id="searchform">
		    	<div class="form-group">
		    		<div class="col-xs-12 col-md-6 col-lg-4">
				    	 <input type="text" class="form-control" name="term" id="term" action="../food/search.php" />
				    </div>
				    <button class="btn btn-primary">
				  Search
				</button>
			    </div>		      	
		  	</form>
	  	</div>
	</div>
</div>

<?php
	if (isset($_GET['term'])){
		echo '<div class="row well" style="display: none">';
		include $_SERVER['DOCUMENT_ROOT'].'/food/search.php';
		echo '</div>';
		//var_dump($foods);
		echo '<div class="row well">';
		echo '<table class="table table-striped"><tr><th>Name</th><th>Description</th><th>Calories</th></tr>';
		for($i=0; $i<count($foods); $i++){
			echo '<tr>';
			echo "<td>".$foods[$i]->getName()."</td>";
			echo "<td>".$foods[$i]->getDescription()."</td>";
			echo "<td>".$foods[$i]->getCalories()."</td></tr>";
			//echo $foods[$i]->getCalories().'<br/>';
		}
	}
	echo '</table>';
	echo '</div>';
?>
</div>