<?php
$scripts="/static/js/select2.min.js,/static/js/foodindex.js";
$stylesheets="/static/css/select2.css";
//include $_SERVER['DOCUMENT_ROOT'].'/includes/header.php';
//include $_SERVER['DOCUMENT_ROOT'].'/auth/functions.php';

?>
<div class="row">
	<div id="error" class="col-xs-12 hide">
	</div>
	<div class="col-xs-12 col-lg-12 well">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
			    <h3>Search  food</h3>
			    <p>Enter a food name to search our database</p>
			</div>
		    <div class="col-xs-4">
			    <button class="btn btn-primary margin-top-md  pull-right " data-toggle="modal" data-target="#myModal">
				  Insert food to database
				</button>
			</div>
		</div>
	    
	  	<div class="row">
		    <form   method="GET"  id="searchform">
		    	<div class="form-group">
		    		<div class="col-xs-12 col-md-6 col-lg-4">
				    	 <input type="hidden" class="form-control" name="foodid" id="foodid"  />
				    </div>
			    </div>
		     	 <input type="hidden" name="function"  id="function" value="searchfood" />
		      	
		  	</form>
	  	</div>
	  	<div class="col-xs-12 padding-top">
			

		</div>
			<div class="col-lg-12" id="content">
				<!--<div class='row border-bottom'><strong><div class='col-xs-3'> <input type="checkbox"  name="eatfood" value="selectall"></div><div class='col-xs-3'>Name</div><div class='col-xs-3'>Description</div><div class='col-xs-3'>Calories</div></strong></div>
				<div class='row'><div class='col-xs-3'> <input type="checkbox"  name="eatfood" value="foodid"></div><div class='col-xs-3'>bob</div><div class='col-xs-3'>bob bbq leg</div><div class='col-xs-3'>500</div></div>-->
			</div>
	</div>
</div>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<!-- model content --> 
		<div class="modal-content" id="form-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class ="modal-title" id="myModalLabel">Add Food</h4>
			</div>
			<div>
				<form method="POST" id="food-form" class="food">
					<fieldset>
						
						<div class="col-xs-12">
							<div class="form-group">
								<label  for ="name">Food Name</label>
								<input type="text" class="form-control" name="name" id="name"/>
							</div>
							<div class="form-group">
								<label for = "description">Description</label>
								<input type="text" class="form-control" name="description" id="description"/>
							</div>
							<div class="form-group">
								<label for ="calories">Calories</label>
								<input type="text" class="form-control" name="calories" id="calories"/> 
							</div>
							<input type="text" name="function" id="function" class="hide" value="addfood"/>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<button  type="submit" class="btn btn-success" id="submit">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<?php 
	if(isset($_SESSION['user_id'])){
		echo'<script> var user_id='.$_SESSION['user_id'].';</script>';
	}
	/*if(isset($_POST['val'])){
		//$value = $_POST['val'];
		echo '<script> var numboxes='.$_POST['val'].';</script>';
	}*/
  include $_SERVER['DOCUMENT_ROOT'].'/includes/footer.php';
?>