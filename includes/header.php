 <!DOCTYPE HTML>
 <html lang="en">
    <head>
    	<title>Yeti Tracker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <script src="https://code.jquery.com/jquery.js"></script>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="/static/css/layout.css">
		<link rel="stylesheet" href="/static/css/select2.css">
		<link rel="stylesheet" href="/static/css/select2-bootstrap.css">
		<!--<link rel="stylesheet" href="/static/css/bootstrap-datetimepicker.min.css">-->
        <!--<link rel="stylesheet" href="/static/css/main.css">-->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script src="/static/js/select2.js" type="text/javascript"></script>
		
		<!--<script src="/static/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>-->
		<?php 
			date_default_timezone_set('America/New_York');
			include_once $_SERVER['DOCUMENT_ROOT'].'/includes/errors.php';
		 	include_once $_SERVER['DOCUMENT_ROOT'].'/auth/functions.php';
		 	include_once $_SERVER[ 'DOCUMENT_ROOT'].'/includes/connections.php';
		 	if (empty($_SESSION)){
		 		    sec_session_start();
		 	}
			if (isset( $scripts)) {
				$scriptarr = explode(',', $scripts);
				if(is_array($scriptarr)){
			    	foreach ($scriptarr as $item) {
			    		echo '<script src="'.$item.'" type="text/javascript"></script>';
			   		 }
			   	}else{
			   		echo '<script src="'.$scripts.'" type="text/javascript"></script>';
			   	}
		   	}
		   	if (isset( $stylesheets)) {
		   		 $stylesheetarr=explode(',', $stylesheets);
		   		if(is_array($stylesheetarr)){
			   		 foreach ($stylesheetarr as $item) {
			    		echo '<link rel="stylesheet" href="'.$item.'"/>';
			   	}
			   	}else{
			   		echo '<link rel="stylesheet" href="'.$stylesheets.'"/>';
			   	}
		   	}
	    ?>
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
    			<div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
      				<div class="navbar-brand" >Yeti Tracker</div>
   					</div>
    				<!-- Collect the nav links, forms, and other content for toggling -->
      				<div class="collapse navbar-collapse">	
      				<?php 
      				$url=strtok($_SERVER["REQUEST_URI"],'?');
      				$arr=explode ( '/' , $url);
      				?>

			          <ul class="nav navbar-nav">
			            <li class="<?php echo (strpos($url,'index.php') !=0 && count($arr) ==2?'active':'');?>"><a href="/index.php">Home</a></li>
			            <li class="<?php echo (strpos($url,'food/index.php') !=0?'active':'');?>"><a href="/food/index.php">Food</a></li>
			            <li class="<?php echo (strpos($url,'exercise/index.php') !=0?'active':'');?>"><a href="/exercise/index.php">Exercise</a></li>
			            <li class="<?php echo (strpos($url,'user/bmi.php') !=0?'active':'');?>"><a href="/user/bmi.php">Bmi</a></li>
			          </ul> 
			          <div class=" pull-right" >
			          	<?php
			          		if (login_check($con) == true) {
							     // $logged = 'in';
			          			
			          			//echo  basename($_SERVER['PHP_SELF']);
			          			echo '<p class="navbar-text">Currently logged in as '.$_SESSION['username'].'</p>';
			          			echo '<a  href="/auth/logout.php" class="margin-top btn btn-default">logout</a>';	
							  } else {
							     // $logged = 'out';
							  	echo '<a  href="/auth/login.php" class="margin-top btn btn-default">login</a>
								      <a  href="/auth/register.php" class="margin-top btn btn-default">Register</a>';
							  }
			          	?>
				      </div>
    			</div><!-- /.navbar-collapse -->
  			</div><!-- /.container-fluid -->
		</nav>
		<div class="container">
