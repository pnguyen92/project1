<div class="row ">
	<div class="col-xs-12">
		<div class="well">
			<h2> Lose Weight With Our Calorie Counter!</h2>
			<p> Our calorie counter app is quick and easy to use. Registering only takes a minute of your time! <a href="/auth/register.php">Get Started Today!</a></p>
		</div>
	</div>
</div>
<div class="row">
	<div class="  col-xs-12 col-sm-3">
		<div class="well">
			<h2>Why Is a Maintaining Healthy Weight Important?</h2>
			<p>Reaching and maintaining a healthy weight is important for overall health and can help you prevent and control many diseases and conditions. If you are overweight or obese, you are at a higher risk of developing serious health problems, including: heart disease, high blood pressure, type 2 diabetes, gallstones, breathing problems, and certain cancers. This is why maintaining a healthy weight is so important: it helps you lower your risk for developing these problems, helps you feel good about yourself, and gives you more energy to enjoy life. </p>
		</div>
	</div>
	<div class=" col-xs-12 col-sm-4 col-sm-offset-1">
		<div class="well">
			<h2>What Factors Contribute To a Healthy Weight?</h2>
			<p>Many factors can contribute to a person’s weight. These factors include environment, family history and genetics, metabolism (the way your body changes food and oxygen into energy), and behavior or habits. Energy balance is also important for maintaining a healthy weight. The amount of energy or calories you get from food and drinks (energy IN) is balanced with the energy your body uses for things like breathing, digesting, and being physically active (energy OUT):</p>
			<ul>
				<li>The same amount of energy IN and energy OUT over time = weight stays the same (energy balance)</li>
				<li>More energy IN than OUT over time = weight gain</li>
				<li>More energy OUT than IN over time = weight loss</li>
			</ul>
			<p>To maintain a healthy weight, your energy IN and OUT don’t have to balance exactly every day. It’s the balance over time that helps you maintain a healthy weight.</p>
			<p>You can reach and maintain a healthy weight if you:</p>
			<ul>
				<li>Follow a healthy diet, and if you are overweight or obese, reduce your daily intake by 500 calories for weight loss</li>
				<li>Are physically active</li>
				<li>Limit the time you spend being physically inactive</li>
			</ul>
		</div>
	</div>
	<div class="  col-xs-12 col-sm-3 col-sm-offset-1">
		<div class="well">
			<h2>A Few Useful Links</h2>
			<ul>
				<li><a href="/food/index.php">Lookup Calories in Common Foods</a></li>
				<li><a href="/exercise/index.php">Lookup Calories Burned From Common Exercises</a></li>
				<li><a href="/user/bmi.php">Calculate Your BMI</a></li>
			</ul>
		</div>
	</div>
</div>