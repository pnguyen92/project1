
//$(document).ready(function (){
jQuery(function($){
    var count=0;
	$('#content').on('click','#checkAll',function(e){//check all checboxes
        console.log("b");
        $('input#check').not(this).prop('checked',this.checked);
    });

    $('#content').on('click','button#send_item',function(e){//this function will save all items selected to database and remove everything to original.
        e.preventDefault();
        var selectedBox=[];
        var servings=[];
        //console.log(count);
        $(':checkbox[name="exercise"]:checked').each(function(){//get the values of call checkboxes
            selectedBox.push(this.value);
        });
        var str="";
        for(var i=0; i<count; i++){
            str="select[name=getValue"+i+"]";
            servings.push($(str).val());
        }
        //var today = new Date().toISOString().slice(0, 19).replace('T', ' ');;
        //console.log(val);
        //console.log(selectedBox);
        //console.log(servings);
        $.ajax({
            type: "GET",
            url: "/exercise/process.php?function=saveItem&",
            data: {selectedBox:selectedBox, user_id:user_id, servings:servings},
            returntype: "json",
            success: function(msg){
                var data = JSON.parse(msg);
                console.log(data);
                    $('#error').addClass("bg-info");
                    $('#error').html(data.msg);
                    $("#error").fadeIn("slow", function() {
                        $(this).removeClass("hide");
                    });
                    setTimeout(function() {
                        $('#error').addClass("hide");
                        $('#error').removeClass("bg-info");
                        $('#content').children().remove();
                        $('#exerciseid').select2("val","");
                    }, 5000);
            },
            error: function(){
                alert("ERROR SOMEWHERE!!");
            }
        });
    });

    $("button#submit").click(function(){//add new exercise to database

        $.ajax({
            type: "get",
            returntype: "json",
            url: "/exercise/process.php",
            data: $('form.exercise').serialize(),
            success: function(msg){
            	var data = JSON.parse(msg);
            	console.log(data);
            	if (data.status!=200){
            		var str="";
            		for(var i=0;i<data.error.length;i++){
            			str=str+data.error[i]+"\n";
            		}
            		alert(str);
            	}else{
	            	$('#myModal').modal('hide');
	            	$('#error').addClass("bg-info");
	            	$('#error').html(data.msg);
	            	$("#error").fadeIn("slow", function() {
					    $(this).removeClass("hide");
					});
					setTimeout(function() {
	 					$('#error').addClass("hide");
	 					$('#error').removeClass("bg-info");
					}, 5000);
				}
                //$("#thanks").html(msg) //hide button and show thank you
                //$("#form-content").modal('hide'); //hide popup
            },
            error: function(){
                alert("an error has occured!");
            }
        });
    });

	$('#exerciseid').select2({//exercise lookup/enter daily exercise
        placeholder: 'Search for exercise',
        multiple: true,
        minimumInputLength: 3,
        ajax: {
            url: "/exercise/process.php?function=searchexercise&",
            dataType: 'json',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    term: term, //search term
                    page_limit: 10 // page size
                };
            },
            results: function (data, page) {
                var newData=[];
                for(var i=0; i<data.length;i++){
                	newData.push({
                		id:data[i].exerciseid,
                		text:data[i].exercisename
                	});
                }
                return { results: newData };
            }
        },
        dropdownCssClass : "bigdrop",
        dropdownAutoWidth : true,
    }); 

    $('#exerciseid').on("change",function(e){//this function will populate the list of selected exercises to choose from 
        e.preventDefault();
        $.ajax({
            type: "GET",
            returntype :"json",
            url: "/exercise/process.php?",
            data: {exerciseid:$("#exerciseid").val(), function:"searchexercise"},//('form#searchform').serialize(),
            success: function(msg){
                var data=JSON.parse(msg);
                /*var exercise = data.exercises;
                var sendtophp=exercise.length
                $.post('index.php', 'val=' + sendtophp, function (response) {
                        //alert(response);
                    });*/
                if (data.status==200){
                    var arr = data.exercises;
                    count=arr.length;
                    console.log(arr);
                    str='<div class="row border-bottom"><strong><div class="col-xs-1"><input type="checkbox" id="checkAll" name="eatAll" value="selectall">&nbspAll</div><div class="col-xs-3">Name</div><div class="col-xs-4">Description</div><div class="col-xs-3">Calories</div><div class="col-xs-1">Increments</div></strong></div>';
                    for(i=0;i<arr.length;i++){
                        var calories=arr[i].calories;
                        var description=arr[i].description;
                        var exerciseid=arr[i].id;
                        var name=arr[i].name;
                        str=str+'<div class="row"><div class="col-xs-1"> <input type="checkbox" id="check" name="exercise" value="'+exerciseid+'"></div><div class="col-xs-3">'+name+'</div><div class="col-xs-4">'+description+'</div><div class="col-xs-3">'+calories+'</div>\
                            <div class="col-xs-1">\
                                <select class="form-control" id="getValue" name="getValue'+i+'">\
                                <option name"select" value="1" id="1">1</option>\
                                <option name"select" value="2" id="2">2</option>\
                                <option name"select" value="3" id="3">3</option>\
                                <option name"select" value="4" id="4">4</option>\
                                </select>\
                            </div>\
                            </div>';
                    }
                    str=str+'<div class="row"><div class="col-xs-3"><button class="btn btn-primary" type="button" id="send_item" name="send_item">Save!</div></div>';
                    $("#content").html(str);
                }
                else {
                    console.log("a");
                    $('#content').children().remove();
                }
            },
            error: function(){
                $("#content").remove();
            }
        });
    });
});