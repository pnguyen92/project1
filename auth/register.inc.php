<?php
include_once $_SERVER[ 'DOCUMENT_ROOT'].'/includes/connections.php';
include_once 'functions.php';
//require_once('recaptchalib.php');

$error_msg = '';
/*
if(isset($_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"] )){
    $privatekey = "6LcKevwSAAAAAIL_xa8IBfqlwmckLCFo7_NsAf-_";
    $resp = recaptcha_check_answer ($privatekey,
        $_SERVER["REMOTE_ADDR"],
        $_POST["recaptcha_challenge_field"],
        $_POST["recaptcha_response_field"]);
*/
   // if (!$resp->is_valid) {
        // What happens when the CAPTCHA was entered incorrectly
        //die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
         //;
        //$error_msg = "The reCAPTCHA wasn't entered correctly. Please try it again" . " (" . $resp->error . ")";
   // } else {
        // Your code here to handle a successful verification
        if (isset($_POST['username'], $_POST['email'], $_POST['password'])){
            // Sanitize and validate the data passed in
            $username = $_POST['username'];
            $email=$_POST['email'];
            $cemail=$_POST['confirm_email'];
            $fname=$_POST['fname'] !=''?$_POST['fname']:NULL;
            $lname=$_POST['lname']!=''?$_POST['lname']:NULL;

            $height=($_POST['feet']*12)+$_POST['inches'];
            $weight=intval($_POST['weight'])!=0?$_POST['weight']:NULL;
            //$goalweight=NULL;
            $goalweight=intval($_POST['goalweight']);//!=0?$_POST['weight']:NULL;

            $gender=$_POST['gender'];
            $activitylv=$_POST['activitylv'];
            $dob=trim($_POST['dob']);

            /*if(strlen($dob)<2){
                if(!is_numeric($dob)){
                    $error_msg .= "Invalid input for DOB";
                }
                else
                  //  $error_msg .= "DOB must be 2 characters long";
                $dob=sprintf("%02d", $dob);
            }
            else if(strlen($dob)==2){
                if(!is_numeric($dob[0]) && ($dob[0]!='-' || $dob[0]!= '+')){
                    $error_msg .= "Invalid input for DOB";
                }
                else
                    //if()
                    if($dob<=0){
                        $error_msg .= "Needs to be greater than 0";
                    }
                    if($dob[0]=='+'){
                        $dob=sprintf("$02d", $dob[1]);
                    }
            }*/

            if($goalweight==NULL){
                $error_msg .= "Please check Goal Weight (non-exist or equals 0 )!";// + $gender + $activitylv;
            }

            //check username format.
            if (!preg_match("/^[a-zA-Z 0-9_]*$/",$username)) {
            //if (!preg_match("/^[a-zA-Z 0-9_]+$/",$username)) {
                $error_msg .= "Only digits, Upper/Lower case letters, underscores, white space are allowed for username"; 
            }

            //$email = filter_var($email, FILTER_VALIDATE_EMAIL);
            if (!isset($email) ) {
                // Not a valid email
                $error_msg .= '<p class="error">The email address you entered is not valid</p>';
            }
            if(!isset($cemail) ){
                //did not confirm email
                $error_msg .= '<p class="error">Please confirm email!</p>';
            }
            else
                if($cemail!=$email){
                //not matched
                    $error_msg .= '<p class="error">Email does not match</p>';
                }

                //check the format of email    
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $error_msg .= "Invalid email format. Ex: myemail@gmail.com"; 
                }

                $cpassword=$_POST['confirmpwd'];
                $password=$_POST['password'];
                //$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

                //check if password is between 6-12 characters long
                if(strlen(trim($password)) < 6 || strlen(trim($password)) > 15){
                    $error_msg .= '<p class="error">Password has to be between 6-15 characters long!</p>';

                }
                //check if password pass the requirements
                if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).+$/', $password)){
                    //error
                    $error_msg .= '<p class="error">Password does not meet requirements, please check again!</p>';
                }

                //check if password matches confirm password
                if($cpassword!=$password){
                    //not matched
                    $error_msg .= '<p class="error">Pleae check your confirm password</p>';
                }
                if (!isset($password) ){
                    // The hashed pwd should be 128 characters long.
                    // If it's not, something really odd has happened
                    $error_msg = $password.'<p class="error">Invalid password configuration.</p>';
                }
                $prep_stmt = "SELECT id FROM users WHERE email = ? LIMIT 1";
                $stmt = $con->prepare($prep_stmt); 
                // check existing email  
                if ($stmt) {
                    $stmt->bind_param('s', $email);
                    $stmt->execute();
                    $stmt->store_result();

                    if ($stmt->num_rows == 1) {
                        // A user with this email address already exists
                        $error_msg .= '<p class="error">A user with this email address already exists.</p>';
                        $stmt->close();
                    }
                //$stmt->close();
                } else {
                    $error_msg .= '<p class="error">Database error Line 39</p>';
                    $stmt->close();
                }

                // check existing username
                $prep_stmt = "SELECT id FROM users WHERE uname = ? LIMIT 1";
                $stmt = $con->prepare($prep_stmt);
                if ($stmt) {
                    $stmt->bind_param('s', $username);
                    $stmt->execute();
                    $stmt->store_result();
                    if ($stmt->num_rows == 1) {
                        // A user with this username already exists
                        $error_msg .= '<p class="error">A user with this username already exists</p>';
                    }
                    $stmt->close();
                } else {

                    $error_msg .= '<p class="error">Database error line 55</p>';
                    $stmt->close();
                }
                
                if ($insert_stmt= $con->prepare("INSERT INTO users (fname,lname,uname, email, password,height,weight,dob,goalweight,male,activitylvl
                    ) VALUES (?,?,?,?, ?, ?,?,?,?,?,?)") and $error_msg == '') {
                    $insert_stmt->bind_param( "sssssiisiid",$fname,$lname, $username, $email, $password,$height,$weight,$dob,$goalweight,$gender,$activitylv);
                    //  Execute the prepared query.
                    //$error_msg.="what";
                    if (! $insert_stmt->execute()) {

                        $error_msg .= 'Registration failure: INSERT';
                        //$error_msg .= "INSERT INTO users (fname,lname,uname,email,password, height, weight, dob, goalweight,mail, activitylv) VALUES ($fname,$lname, $username, $email, $password,$height,$weight,$dob,$goalweight,$gender,$activitylv)";
                    }

                }
                    //echo  mysqli_errno($insert_stmt);
                if (login($email, $password, $con) == true) {
                    //Login success 
                    header('Location: ../index.php');
                }         
            }
        //}
    //}
?>