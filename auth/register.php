<?php
$scripts="/static/js/bootstrap-datepicker.js";
$stylesheets="/static/css/datepicker3.css";
include $_SERVER['DOCUMENT_ROOT'].'/includes/header.php';
//include $_SERVER['DOCUMENT_ROOT'].'/auth/functions.php';
include $_SERVER['DOCUMENT_ROOT'].'/auth/register.inc.php';


?>
<div class="row well">
    <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        <h1>Register with us</h1>
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
       if(isset($_POST['username'], $_POST['email'], $_POST['password'])){
            $uname= $_POST['username'];
            $email=$_POST['email'];
            $password=$_POST['password'];
            $fname=$_POST['fname'];
            $lname=$_POST['lname'];
            //$height=$_POST['height'];
            $weight=$_POST['weight'];
            $goalweight=$_POST['goalweight'];  
            //$dob=$_POST['dob'];
        }else{
            $uname="";
            $fname="";
            $lname="";
            //$height="";
            $weight="";
            $goalweight="";
            $email="";
            $password="";
            //$dob=NULL;  
        }
       // print_r($_POST);

        ?>
        <ul>
            <li>Usernames may contain only digits, upper and lower case letters and underscores</li>
            <li>Emails must have a valid email format</li>
            <li>Passwords must be between 6-15 characters long</li>
            <li>Passwords must contain
                <ul>
                    <li>At least one upper case letter (A..Z)</li>
                    <li>At least one lower case letter (a..z)</li>
                    <li>At least one number (0..9)</li>
                </ul>
            </li>
            <li>Your password and confirmation must match exactly</li>
        </ul>

        <form  method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>"class="form-horizontal" name="registration_form">
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">First Name:</label>
                <div class="col-xs-10 col-md-6 col-lg-6">
                    <input type='text' name='fname' id='fname' value="<?php echo $fname ?> " class="form-control" />
                </div>
            </div>
        	<div class="form-group">
                <label for="username" class="col-sm-2 control-label">Last Name:</label>
                <div class="col-xs-10 col-md-6 col-lg-6">
                    <input type='text' name='lname' id='lname' value="<?php echo $lname ?> " class="form-control" />
                 </div>
            </div>
            <div class="form-group required">
        		<label for="username" class="col-sm-2 control-label" required="required">Username:</label>
        		<div class="col-xs-10 col-md-6 col-lg-6">
	             	<input type='text' name='username' id='username' value="<?php echo $uname ?> " class="form-control" />
	             </div>
	        </div>
            <div class="form-group required">
            	<label for="email" class="col-sm-2 control-label" required="required">Email:</label>
            	<div class="col-xs-10 col-md-6 col-lg-6">
		            <input type="text" name="email" id="email" value="<?php echo $email?>"  class="form-control"/>
		        </div>
	        </div>
            <div class="form-group required">
                <label for="confirm_email" class="col-sm-2 control-label" required="required">Confirm email:</label>
                <div class="col-xs-10 col-md-6 col-lg-6">
                    <input type="text" value="<?php echo $email?>"  name="confirm_email" id="confirm_email"  class="form-control" />
                </div>
            </div>
            <div class="form-group required">
            	<label for="password" class="col-sm-2 control-label" required="required">Password:</label>
            	<div class="col-xs-10 col-md-6 col-lg-6">
	                <input type="password" name="password" value="<?php echo $password?>"  id="password"  class="form-control"/>
	            </div>
	        </div>
            <div class="form-group required">
            	<label for="confirmpwd" class="col-sm-2 control-label" required="required">Confirm password:</label>
            	<div class="col-xs-10 col-md-6 col-lg-6">
	            	<input type="password" name="confirmpwd" id="confirmpwd"  class="form-control" />
	         	</div>
	        </div>
        
            <div class="form-group required">
                <label for="feet" class="col-sm-2 control-label" required="required">Height:</label>
                <div class="col-xs-4 col-md-2 col-lg-2">
                   <select name="feet" class="form-control ">
                    <option value="0" > Select one </option>
                    <option value="4" > 4 </option>
                    <option value="5" > 5</option>
                    <option value="6" > 6 </option>
                    <option value="7" > 7 </option>
                    </select> 
                </div>
                <div class="col-xs-1 col-md-1 col-lg-1">
                    Ft
                </div>
                <div class="col-xs-4 col-md-2 col-lg-2">
                    <select name="inches"  class="form-control ">
                    <option value="0" >0</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                    <option value="6" >6</option>
                    <option value="7" >7</option>
                    <option value="8" >8</option>
                    <option value="9" >9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    </select>
                 </div>
                 <div class="col-xs-1 col-md-1 col-lg-1">
                    Inches
                </div>
              
            </div>

            <div class="form-group required">
                <label for="weight" class="col-sm-2 control-label" required="required">Weight:</label>
                <div class="col-xs-10 col-md-6 col-lg-6">
                    <input type='text' name='weight' id='weight' value="<?php echo $weight ?> " class="form-control" />
                 </div>
            </div>
            <div class="form-group required">
                <label for="goalweight" class="col-sm-2 control-label" required="required">GoalWeight:</label>
                <div class="col-xs-10 col-md-6 col-lg-6">
                    <input type='text' name='goalweight' id='goalweight' value="<?php echo $goalweight ?> " class="form-control" />
                </div>
            </div>

            <div class ="form-group required">
                <label for="gender" class="col-xs-2 col-sm-2 control-label" required="required">Gender:</label>
                <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
                   <select name="gender" class="form-control ">
                    <option value="0" > Male </option>
                    <option value="1" > Female </option>
                    </select> 
                </div>
                <label for="activitylv" class="col-xs-2 col-sm-2 control-label" required="required">Activity Level:</label>
                <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
                   <select name="activitylv" class="form-control ">
                    <option value="0.2" > Very light </option>
                    <option value="0.3" > Light </option>
                    <option value="0.4" > Moderate </option>
                    <option value="0.5" > Heavy </option>
                    </select> 
                </div>
            </div>

            <!--<div class ="form-group required">
                <label for="gender" class="col-sm-2 control-label" required="required">Gender:</label>
                <div class="col-xs-12 col-md-2 col-lg-2">
                   <select name="gender" class="form-control ">
                    <option value="0" > Male </option>
                    <option value="1" > Female </option>
                    </select> 
                </div>
            </div>

            <div class ="form-group required">
                <label for="activitylv" class="col-sm-2 control-label" required="required">Activitiy Level:</label>
                <div class="col-xs-12 col-md-2 col-lg-2">
                   <select name="activitylv" class="form-control ">
                    <option value="1.2" > Very light </option>
                    <option value="1.375" > Light </option>
                    <option value="1.55" > Moderate </option>
                    <option value="1.725" > Heavy </option>
                    <option value="1.9" >Very Heavy </option>
                    </select> 
                </div>
            </div>-->
            
            <div class="form-group required">
                <label for="dob" class="col-sm-2 control-label" required="required" >Date of Birth:</label>
                <div class="col-xs-4 col-md-2 col-lg-2">
                    <div class="input-group date">
                        <input type="text" name="dob" id="dob" class="form-control">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-th"></i>
                        </span>
                    </div>

                </div>

            </div>
            <script type="text/javascript">
                $('.input-group.date').datepicker({
                //$('#test').datepicker({
                    format: "yyyy-mm-dd",
                    todayBtn: "linked",
                    autoclose: true,
                    todayHighlight: true
                });
            </script> 
  
            <!--<div class="col-sm-offset-2 col-xs-10 col-md-6 col-lg-3">
                   <?php 
                        //require_once('recaptchalib.php');
                        //$publickey = "6LcKevwSAAAAABl1hYX8nhHdBrTO54Ec4ZPDUx0m"; // you got this from the signup page
                        //echo recaptcha_get_html($publickey);
                    ?>
            </div>-->
            <div class="form-group">
            	<div class="col-sm-offset-3 col-sm-10">
            	<input type="submit" value="Register" class="btn btn-primary"  /> 
                </div>
            </div>

        </form>
        
        <p>Return to the <a href="login.php">login page</a>.</p>
</div>


<?php 
  include $_SERVER['DOCUMENT_ROOT'].'/includes/footer.php';
?>
