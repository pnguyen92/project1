
<?php
$scripts="";
$stylesheets="";
include $_SERVER['DOCUMENT_ROOT'].'/includes/header.php';
//sec_session_start();
if (!empty($_POST['email']) &!empty( $_POST['password'])) {
    $email = $_POST['email'];
    $password = $_POST['password']; // The hashed password.
    if (!login($email, $password, $con) == true) {
           $errormsg ="Invalid User name or password.";
    }
  } 

?>
  <div class="row ">
    <div class="col-xs-12">
    </div>
    <div class="col-xs-12 well ">
      <?php
        if(isset($errormsg)){
          echo $errormsg;
        }
      ?>
  	<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" class=" form-horizontal " name="registration_form">
        	<div class="form-group">
        		<label for="username" class="col-lg-offset-2 col-sm-2 control-label">Username or email:</label>
        		<div class="col-xs-10 col-md-6 col-lg-4">
	             	<input type='text' name='email' id='email' class="form-control" />
	             </div>
	        </div>
            <div class="form-group">
            	<label for="password" class="col-lg-offset-2 col-sm-2 control-label">Password:</label>
            	<div class="col-xs-10 col-md-6 col-lg-4">
	                <input type="password" name="password"  id="password"  class="form-control"/>
	            </div>
	        </div>

            <div class="form-group">
            	<div class="col-sm-offset-2 col-lg-offset-5 col-sm-10 col-md-6 col-lg-4">
            	<input type="submit" value="Login" class="btn btn-primary"  /> 
            </div>
        </form>
      </div>
  </div>
<?php 
  include $_SERVER['DOCUMENT_ROOT'].'/includes/footer.php';
?>