<?php
$scripts="";
$stylesheets="";
include $_SERVER['DOCUMENT_ROOT'].'/includes/header.php';
?>
<div class="row well">
  <div class="col-xs-12">
    <h2>Welcome to Our BMI Calculator</h2>
    <p>The body mass index (BMI), or Quetelet index, is a measure of relative weight based on an individual's mass and height. The BMI is used in a wide variety of contexts as a simple method to assess how much an individual's body weight departs from what is normal or desirable for a person of his or her height.</p>
    <div class="panel panel-default col-xs-5">
      <div class="panel-body">
        <form role="form" method="post">
          <div class="form-group">
            <label>Feet</label>
            <select name="bmiFeet">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
            </select>
            <br />
            <label>Inches</label>
            <select name="bmiInches">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
              <option>11</option>
            </select>
            <br />
            <label>Weight</label>
            <input type="text" name="bmiWeight">
            <br />
            <button type="submit" class="btn btn-default">Submit</button>
            
          </div>
        </form>
      </div>
    </div>
    <div class="panel panel-default col-xs-5 col-xs-offset-1">
      <div class="panel-body">
        <table class="table table-striped">
          <tr>
            <th>Category</th>
            <th>BMI Range</th>
          </tr>
          <tr>
            <td>Severe Thinness</td>
            <td>Less Than 16</td>
          </tr>
          <tr>
            <td>Moderate Thinness</td>
            <td>16 - 16.99</td>
          </tr>
          <tr>
            <td>Mild Thinness</td>
            <td>17 - 18.5</td>
          </tr>
          <tr>
            <td>Normal</td>
            <td>18.5 - 24.99</td>
          </tr>
          <tr>
            <td>Overweight</td>
            <td>25 - 29.99</td>
          </tr>
          <tr>
            <td>Obese Class I</td>
            <td>30 - 34.99</td>
          </tr>
          <tr>
            <td>Obese Class II</td>
            <td>35 - 39.99</td>
          </tr>
          <tr>
            <td>Obese Class III</td>
            <td>Greater Than 40</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="panel panel-default col-xs-12">
      <div class="panel-body">
        <?php
              if(isset($_POST['bmiFeet'], $_POST['bmiInches'], $_POST['bmiWeight']))
              {
                $feet=$_POST['bmiFeet'];
                $inches=$_POST['bmiInches'];
                $weight=$_POST['bmiWeight'];
                $inches=$inches+($feet*12);
                $bmi=($weight/($inches*$inches))*703;
                echo "Your current BMI is <b>".round($bmi, 2)."</b>. Please consult the table above to see in which range you fall in.";
              }
        ?>

      </div>
    </div>
      </div>
</div>

<?php 
  include $_SERVER['DOCUMENT_ROOT'].'/includes/footer.php';
?>