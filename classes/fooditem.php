<?php 
class FoodItem
{
	private $foodID;
	private $userID;
	private $date;
	private $name;
	private $description;
	private $calories;
	private $servings;

	public function getFoodID()
	{
		return $this->foodID;
	}

	public function setName($name)
	{
		$this->name=$name;
	}
	public function setDescription($description)
	{
		$this->description=$description;
	}

	public function setCalories($calories)
	{
		$this->calories=$calories;
	}

	public function getName()
	{
		return $this->name;
	}
	public function getDescription()
	{
		return $this->description;
	}

	public function getCalories()
	{
		return $this->calories;
	}

	public function setFoodID($idFood)
	{
		$this->foodID=$idFood;
	}

	public function getUserID()
	{
		return $this->userID;
	}

	public function setUserID($idUser)
	{
		$this->userID=$idUser;
	}

	public function getDate()
	{
		return $this->date;
	}

	public function setDate($dateEntered)
	{
		$this->date=$dateEntered;
	}

	public function getServings()
	{
		return $this->servings;
	}

	public function setServings($servings)
	{
		$this->servings=$servings;
	}
}
?>