<?php 
class Exercise
{
	private $description;
	private $calories;
	private $name;

	function _construct($n, $cal, $desc)
	{
		$this->name=setName($n);
		$this->calories=setCalories($cal);
		$this->description=setDescription($desc); 
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($desc)
	{
		$this->description=$desc;
	}

	public function getCalories()
	{
		return $this->calories;
	}

	public function setCalories($cal)
	{
		$this->calories=$cal;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($n)
	{
		$this->name=$n;
	}
}
?>