<?php

class User
{
	private $username;
	private $currweight;
	private $goalweight;
	private $height;
	private $gender;
	private $dob;
	private $activitylvl;
	private $goalweekly;
	private $foods;
	private $exercises;

	public function setUsername($name)
	{
		$this->username = $name;
	}
	
	public function getUsername()
	{
		return $this->username;
	}
	
	public function setCurrWeight($weight)
	{
		$this->currweight = $weight;
	}
	
	public function getCurrWeight()
	{
		return $this->currweight ;
	}
	
	public function setGoalWeight($goal)
	{
		$this->goalweight = $goal;
	}
	
	public function getGoalWeight()
	{

		return $this->goalweight;


	}
	
	public function setHeight($height)
	{
		$this->height = $height;
	}
	
	public function getHeight()
	{

		return $this->height;

		return $this->height ;

	}
	
	public function setGender($gen)
	{
		$this->gender = $gen;
	}
	
	public function getGender()
	{

		return $this->gender;

		return $this->gender ;

	}
	
	public function setDOB($date)
	{
		$this->dob = $date;
	}
	
	public function getDOB()
	{
		return $this->dob;
	}
	
	public function setActivityLvl($level)
	{
		$this->activitylvl = $level;
	}
	
	public function getActivityLvl()
	{
		return $this->activitylvl;
	}
	
	public function setWeeklyGoal($goal)
	{
		$this->goalweekly = $goal;
	}
	
	public function getWeeklyGoal()
	{
		return $this->goalweekly;

	}
	public function getbmr(){//returns bmr
		/*W = weight in kilograms (weight (lbs)/2.2) =weight in kg
		H = height in centimeters (inches x 2.54) =height in cm
		A = age in years

		Men: BMR=66.47+ (13.75 x W) + (5.0 x H) - (6.75 x A)
		Women: BMR=665.09 + (9.56 x W) + (1.84 x H) - (4.67 x A)
		http://www.bodybuilding.com/fun/calorie-know-how-get-equation-right-to-get-results.htm*/
		$ge=$this->getage();
		$w=$this->tokg($this->getCurrWeight());
		$h=$this->tocm($this->getheight());
		if($this->getGender()==1){
			$bmr=66.47+ (13.75 * $w) + (5.0 * $h) - (6.75 * $this->getage());
		}else{
			$bmr=665.09+ (9.56 * $w) + (1.84 * $h) - (4.67 * $this->getage());
		}
		return round($bmr);
	}
	public function tokg($lbs){
		return $lbs/2.2;
	}
	public function tocm($inches){
		return $inches*2.54;
	}
	public function getactivitycal($bmr){//this function returns the aditional calories a user can burn based on activity level
		return $bmr*$this-> getActivityLvl();
	}
	public function getallowancecalories(){
		$bmr=$this->getbmr();
		$subtotal=($bmr*$this->getActivityLvl())-500;
		return round($subtotal);
	}
	public function getage(){
		return date_diff(date_create( $this->dob), date_create('today'))->y;
	}
	public function getFoods()
	{
		return $this->foods;
	}
	public function setFoods($arr)
	{
		$this->foods = $arr;
	}

	public function getExercises()
	{
		return $this->exercises;
	}

	public function setExercises($exercises)
	{
		$this->exercises=$exercises;
	}
}
?>