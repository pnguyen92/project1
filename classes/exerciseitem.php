<?php 
class ExerciseItem
{
	private $exerciseID;
	private $userID;
	private $date;
	private $name;
	private $description;
	private $calories;
	private $increments;

	public function getExerciseID()
	{
		return $this->exerciseID;
	}

	public function setExerciseID($idExer)
	{
		$this->exerciseID=$idExer;
	}

	public function getUserID()
	{
		return $this->userID;
	}

	public function setUserID($idUser)
	{
		$this->userID=$idUser;
	}

	public function getDate()
	{
		return $this->date;
	}

	public function setDate($dateEntered)
	{
		$this->date=$dateEntered;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getCalories()
	{
		return $this->calories;
	}

	public function setCalories($calories)
	{
		$this->calories = $calories;
	}

	public function getIncrements()
	{
		return $this->increments;
	}

	public function setIncrements($increments)
	{
		$this->increments=$increments;
	}
}
?>