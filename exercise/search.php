<?php
	include_once $_SERVER[ 'DOCUMENT_ROOT'].'/includes/connections.php';
	if(isset($_GET['term'])){
		$name=$_GET['term'];
		$sql = "SELECT * FROM exercise WHERE name LIKE '%".$name."%'";
		$result = $con->query($sql);
		$return_arr= array();
		$exercises = array();
			while($row = $result->fetch_assoc()){
				$return_arr[]= array('exercisename'=> $row['name'], 'exerciseid'=>$row['id']);
				$exercise = new exercise();
	           	$exercise->setName($row['name']);
	           	$exercise->setCalories($row['calories']);
	           	$exercise->setDescription($row['description']);
	            array_push($exercises, $exercise);
			}
		$result->close();
		echo json_encode($return_arr);
	}
	if(isset($_GET['exercisestr'])){
		$str=$_GET['exercisestr'];
		$sql = "SELECT * FROM exercise WHERE id IN (".$str.")";
		$result = $con->query($sql);
		echo '<table class="table">';
			echo '<thead>';
				echo '<tr>';
					echo '<th><input type ="checkbox" id="checkAll"></th>';
					echo '<th>Name</th>';
					echo '<th>Calories</th>';
					echo '<th>Description</th>';
				echo '<tr>';
			echo '</thead>';
			echo '<tbody>';
			while($row = $result->fetch_assoc()){
				echo '<tr>';
				echo '<td><input class="doAll" type="checkbox" id="checking"></td>';
				echo '<td>'.$row['name'].'</td>';
				echo '<td>'.$row['calories'].'</td>';
				echo '<td>'.$row['description'].'</td>';
				echo '</tr>';
			}
			echo '</tbody>';
		echo '</table>';
	}
	/*else{
		die();
	}*/
?>