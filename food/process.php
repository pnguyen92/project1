<?php
include  $_SERVER[ 'DOCUMENT_ROOT'].'/includes/errors.php';
require $_SERVER[ 'DOCUMENT_ROOT'].'/includes/connections.php';
//echo "bob";

//print_r($con);
if (!empty($_GET['function'])) {

	$var=$_GET['function']($con);
	echo $var;
}
	function addfood($con){
		$name = strip_tags($_GET['name']);
		$description = strip_tags($_GET['description']);
		$calories = strip_tags($_GET['calories']);
		$response = new stdClass();
		$e =array();
		
		if(empty($name)){
		array_push($e," a name was not entered");
		}
		if(empty($description)){
			array_push($e,"a description was not entered");
		}
		if(empty($calories)){
			array_push($e,"calories was not entered");
		}else{
			if(!is_numeric($calories)){
				array_push($e," calories must be numeric");
			}
		}
		if (count($e) ==0){
			$sql="INSERT INTO food (name, description, calories) VALUES (?, ?, ?)";
			$insert_stmt= $con->prepare($sql);
			if ( $insert_stmt) {
            	$insert_stmt->bind_param( "sss", $name, $description, $calories);
            }
            	//  Execute the prepared query.
            if (! $insert_stmt->execute()) {
                array_push($e,"Internal Server error.");
                $response->error=$e;
				$response->status=500;
				return json_encode($response);
            }
            $response->msg="This food is now available in our food database.";
            $response->status=200;
            return json_encode($response);
		}else{
			$response->error=$e;
			$response->status=500;
			return json_encode($response);
		}
		
	}
	function searchfood($con){

		if(isset($_GET['term'])){
			$name=$_GET['term'];
			$sql = "SELECT * FROM food WHERE name LIKE '%".$name."%'";
			$result = $con->query($sql);
			$return_arr= array();
				while($row = $result->fetch_assoc()){
					$return_arr[]= array('foodname'=> $row['name'], 'foodid'=>$row['foodid']);
				}
			$result->close();
			return json_encode($return_arr);
		}
		if(isset($_GET['foodid'])){
			$str=$_GET['foodid'];
			$sql = "SELECT * FROM food WHERE foodid IN (".$str.")";
			$result = $con->query($sql);

			$response = new stdClass();
			if(!$result){
					$response->status = 500;
					$response->error="Invalid SQL query";
					return json_encode($response);
			}
				while($row = $result->fetch_assoc()){

					$food[]=$row;
				}
				$response->status = 204;
				if(empty($food)){
					$response->status = 204;
					$response->error="No foods were selected.";
				}
				else{
					$response->foods=$food;
					$response->status=200;
				}
				return json_encode($response);
		}

	}

	function saveItem($con){
		$array = $_GET['selectedBox'];
		$response = new stdClass();
		$user_id = $_GET['user_id'];
		$servings= $_GET['servings'];


		if(isset($array,$user_id,$servings)){
			$count=count($array);
			$i=1;
			$insert = "INSERT INTO fooditem (foodid,userid,servings,dateentered) VALUES ($array[0],$user_id,$servings[0],now())";
			while($i<$count){
				$insert .= ",(".$array[$i].",".$user_id.",".$servings[$i].",now())";
				$i++;
			}
			$result = $con->query($insert);
			if($result){
				$response->msg="Items saved! Thank you!";
				$response->status=200;
				return json_encode($response);
			}
			else{
				$response->msg="Invalid Query!";
				$response->status=204;
				return json_encode($response);
			}
		}
		$response->error="Invalid User/Food not chosen";
		$response->status=204;
		return json_encode($response);
	}
?>